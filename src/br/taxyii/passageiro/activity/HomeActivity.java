package br.taxyii.passageiro.activity;

import java.io.IOException;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

public class HomeActivity extends SherlockActivity implements Runnable {
	private int SOLICITAR_TAXI = 1;
	private int CONFIG = 2;
	private int SAIR = 3;
	private Button solicitar_taxi;
	private Button configuracao;
	private boolean gcm_enviado = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_home);

		//perfil = (Button) findViewById(R.id.btnPerfil);
		//sobre = (Button) findViewById(R.id.btnSobre);
		configuracao = (Button) findViewById(R.id.btnConfig);
		solicitar_taxi = (Button) findViewById(R.id.btnSolicitar);
				
		configuracao.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent("PASSAGEIRO_CONFIG"));
			}
		});
		
		solicitar_taxi.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent("ENDERECO_FAVORITO"));
			}
		});

		new Thread(this).start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		SubMenu subMenu1 = menu.addSubMenu("Menu");
		subMenu1.add(0, SOLICITAR_TAXI, 0, "Solicitar taxi");
		subMenu1.add(0, CONFIG, 0, "Configuração");
		subMenu1.add(0, SAIR, 0, "Sair");

		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_drawer);

		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		int opcao = item.getItemId();

		if (SOLICITAR_TAXI == opcao) {
			startActivity(new Intent("ENDERECO_FAVORITO"));
		}

		if (SAIR == opcao) {
			UsuarioModel usuario = new UsuarioDAO(getBaseContext()).get();
			new UsuarioDAO(getBaseContext()).Remover(usuario.getId());
			finish();
		}
		if(CONFIG == opcao){
			startActivity(new Intent("PASSAGEIRO_CONFIG"));
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void run() {
		while (gcm_enviado) {
			String enviado = new UsuarioModel().gcmJaEnviado(getBaseContext());
			
			if(enviado == null){
				try {
					new GcmUtils(getBaseContext()).register(GcmUtils.SENDER_ID_PASSAGEIRO);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}else if (enviado.equals("N")) {
				new UsuarioModel().gcmRegister(getBaseContext());
				gcm_enviado = true;			
			} 
		}
	}

}
