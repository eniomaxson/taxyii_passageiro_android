package br.taxyii.passageiro.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.taxyii.library.adapter.ConfiguracaoAdapter;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ConfiguracaoActivity extends SherlockFragmentActivity implements OnItemClickListener{
	
	private ArrayList<ArrayList<Object>> menu;
	private ListView list_menu;
	private ConfiguracaoAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_configuracao);
		
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		list_menu = (ListView) findViewById(R.id.list_menu);
		list_menu.setOnItemClickListener(this);
		
		menu = new ArrayList<ArrayList<Object>>();
		
		ArrayList<Object> perfil = new ArrayList<Object>();
		perfil.add(R.drawable.ic_action_person);
		perfil.add("Perfil");
		
		ArrayList<Object> sobre = new ArrayList<Object>();
		sobre.add(R.drawable.ic_action_about);
		sobre.add("Sobre");
		
		ArrayList<Object> notificao = new ArrayList<Object>();
		notificao.add(R.drawable.ic_action_volume_on);
		notificao.add("Notifição");
		
		menu.add(notificao);
		menu.add(perfil);
		menu.add(sobre);
		
		adapter = new ConfiguracaoAdapter(getBaseContext(), menu);
		
		list_menu.setAdapter(adapter);
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int opcao = item.getItemId();

		if (opcao == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		
		ArrayList<Object> item = (ArrayList<Object>) adapter.getItem(position);
		
		int opcao = (Integer) item.get(0);
		
		if(opcao == R.drawable.ic_action_volume_on){
			
		}
		
		if(opcao == R.drawable.ic_action_about){
			AndroidUtils.showAlertDialogComIntencao(this, "Sobre","Trabalho Conclusão do Curso Sistemas de Informação", null);
		}
		
		if(opcao == R.drawable.ic_action_person){
			startActivity(new Intent("PERFIL_PASSAGEIRO"));
		}
	}

}
