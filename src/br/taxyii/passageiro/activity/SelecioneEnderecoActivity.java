package br.taxyii.passageiro.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import br.taxyii.library.fragment.ListaEnderecoFragment;
import br.taxyii.library.fragment.ListaEnderecoProximoFragment;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class SelecioneEnderecoActivity extends SherlockFragmentActivity {

	private ListaEnderecoFragment endereco_favorito;
	private ListaEnderecoProximoFragment endereco_proximo;
    ViewPager mViewPager;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setContentView(R.layout.activity_seleciona_endereco);
		
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
		
		mViewPager = (ViewPager) findViewById(R.id.pager);
        
	    mViewPager.setAdapter(mAppSectionsPagerAdapter);
        
	    mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getSupportActionBar().setSelectedNavigationItem(position);
            }
        });
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		ActionBar bar = getSupportActionBar();
		
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		Tab t1 = bar.newTab();
		Tab t2 = bar.newTab();
		
		t1.setIcon(R.drawable.ic_action_important);
		t2.setIcon(R.drawable.ic_action_location_searching);
		
		endereco_favorito = new ListaEnderecoFragment();
		endereco_proximo = new ListaEnderecoProximoFragment();	
		
		endereco_favorito.setAcaoConfirmar("SOLICITAR_TAXI");
		endereco_proximo.setAcaoConfirmar("SOLICITAR_TAXI");
		
		t1.setTabListener(new TabselectionListener(endereco_favorito));
		t2.setTabListener(new TabselectionListener(endereco_proximo));		
		
		bar.addTab(t1);
		bar.addTab(t2);
		
	}

	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int opcao = item.getItemId();

		if (opcao == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
		}
		return super.onMenuItemSelected(featureId, item);
	}

	
	private class TabselectionListener implements TabListener {

		private Fragment frag;

		public TabselectionListener(Fragment frag) {
			this.frag = frag;
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			 mViewPager.setCurrentItem(tab.getPosition());
			 if(tab.getPosition() == 0){
				 getSupportActionBar().setTitle("Meus Endereços");
			 }else{
				 getSupportActionBar().setTitle("Minha Localização");
			 }
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {

		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {

		}

	}
	
	public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                	return endereco_favorito;
                case 1:
                	return endereco_proximo;
                default:
                	return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }
    }
}
