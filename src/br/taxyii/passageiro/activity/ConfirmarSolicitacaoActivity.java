package br.taxyii.passageiro.activity;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.taxyii.library.fragment.ConfirmaSolicitacaoFragment;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ConfirmarSolicitacaoActivity extends SherlockFragmentActivity {
	
	private ConfirmaSolicitacaoFragment confirmSolicitacao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		setContentView(R.layout.activity_confirmar_solicitacao);

		confirmSolicitacao = (ConfirmaSolicitacaoFragment) getSupportFragmentManager().findFragmentById(R.id.viewConfirmacao);
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int opcao = item.getItemId();

		if (opcao == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
		}
		
		return super.onMenuItemSelected(featureId, item);
	}

}
