package br.taxyii.passageiro.activity;


import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.fragment.LoginFragment;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.transacao.TRegistrarGCM;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class LoginActivity extends SherlockFragmentActivity {

	protected LoginButton faceBookLogin;
	private GraphUser usuario;
	private LoginFragment loginView;
	private Intent intencao;
	private UsuarioModel usuarioModel;
	private TgetDados getdado;
	private boolean faceOk = false;
	private boolean dadoVerificado = false;
	
	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);

		getSupportActionBar().hide();

		new Thread(new TRegistrarGCM(GcmUtils.SENDER_ID_PASSAGEIRO,getBaseContext())).start();

		setContentView(R.layout.activity_login);

		intencao = new Intent("PASSAGEIRO_HOME");
		
		loginView = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentLogin);
		
		verificaUsuarioLogado();
		faceBookLogin = (LoginButton) findViewById(R.id.login_facebook);
		
		if(Session.getActiveSession() == null)
			faceBookLogin.setReadPermissions(Arrays.asList("user_location","email"));
		

			faceBookLogin.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {					
				@Override
				public void onUserInfoFetched(GraphUser user) {				
					usuario =  user;
					updateUI();
					faceOk = true;
				}
			});

		
		loginView.setIntentPrimeiroAcesso("NOVO_PASSAGEIRO");
		loginView.setIntencao(intencao);
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		if(dadoVerificado && faceOk)
		{
			Session session = Session.getActiveSession();
		    
		    session.closeAndClearTokenInformation();
		    
			finish();
		}
		
	}
	
	public void verificaUsuarioLogado()
	{
		usuarioModel = new UsuarioDAO(getBaseContext()).get();
		
		if(usuarioModel != null)
		{
			startActivity(new Intent("PASSAGEIRO_HOME"));
			Session session = Session.getActiveSession();
			if(session != null)
				session.closeAndClearTokenInformation();
			
			finish();
		}
	}
	
	private void updateUI() {
		Session session = Session.getActiveSession();
		boolean enableButtons = (session != null && session.isOpened());

		if (enableButtons && usuario != null) {
			
			String nome = usuario.getFirstName();
			
			String sobrenome = usuario.getLastName();
			
			String sexo = ((String) usuario.getProperty("gender")).equals("male") ? "M": "F";
						
			String email = (String) usuario.asMap().get("email");
			
			usuarioModel = new UsuarioModel(nome,sobrenome,sexo,email,"","");
			
			getdado = new TgetDados();
			
			getdado.email = email;
			
			new DefaultTask(this, getdado, "Aguarde..", "Verificando acesso.", 0).execute();
		}
	}
	
	private class TgetDados implements ITransacao{
		
		public String email;
		public String response;
		
		@Override
		public void executar() throws Exception {
			response = usuarioModel.getUsuarioFromEmail(email);
		}

		@Override
		public void atualizarView() {
			if(response != null){
				try{
					JSONObject j = new JSONObject(response);
					if(!j.has("error"))
					{
						dadoVerificado = true;
						Intent i = new Intent("PASSAGEIRO_HOME");
						usuarioModel = (UsuarioModel) usuarioModel.toObject(new JSONObject(j.getString("data")));
						new UsuarioDAO(getBaseContext()).inserir(usuarioModel);
						startActivity(i);
					}else{
						Intent i = new Intent("NOVO_PASSAGEIRO");
					
						i.putExtra("facebook", usuarioModel.toJson());
					
						startActivity(i);
					}
				}catch(JSONException e){
					e.printStackTrace();
				}
			}else{
				AndroidUtils.showSimpleToast(LoginActivity.this, R.string.error_conexao, Toast.LENGTH_LONG);				
			    
				Session session = Session.getActiveSession();
			    
			    session.closeAndClearTokenInformation();
			    
			    faceOk = false;
			}
		}
		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,resultCode, data);
	}
}
