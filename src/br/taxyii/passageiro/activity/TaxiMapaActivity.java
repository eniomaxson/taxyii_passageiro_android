package br.taxyii.passageiro.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.fragment.ProgressFragment;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class TaxiMapaActivity extends SherlockFragmentActivity implements
		OnMapClickListener, OnCameraChangeListener, OnConnectionFailedListener,
		ConnectionCallbacks, ITransacao {

	protected GoogleMap map;
	private SupportMapFragment mapFragment;
	public HashMap<String, JSONObject> motoristas;
	private Location mCurrentLocation;
	private LocationClient mLocationClient;
	private String response;
	private View mapView;
	private ProgressFragment pgFragment;
	private Button btnSolicitarTaxi;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_taxi_mapa);

		pgFragment = (ProgressFragment) getSupportFragmentManager().findFragmentById(R.id.pgMapBar);

		mLocationClient = new LocationClient(this, this, this);
		mLocationClient.connect();
		
		mapView = findViewById(R.id.mapView);
		
		btnSolicitarTaxi = (Button) findViewById(R.id.btnSolicitarTaxi);
		
		mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

		map = mapFragment.getMap();

		// Configura o tipo do mapa

		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

		pgFragment.showProgress(true, mapView);
		
		new DefaultTask(this, this).execute();

		// Eventos
		map.setOnMapClickListener(this);
		
		btnSolicitarTaxi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				mCurrentLocation = mLocationClient.getLastLocation();
				
				double latitude = mCurrentLocation.getLatitude();
				double longitude = mCurrentLocation.getLongitude();
				
				Intent i = new Intent("SOLICITAR_TAXI");
				
				i.putExtra("latitude", latitude);
				
				i.putExtra("longitude", longitude);

				startActivity(i);
			}
		});
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mLocationClient.connect();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mLocationClient.disconnect();
	}

	protected void configureMap(List<JSONObject> taxistas) {

		if (map != null) {

			motoristas = new HashMap<String, JSONObject>();

			for (JSONObject ponto : taxistas) {
				try {
					String posicao = ponto.getString("posicao_atual");
					String[] separaPontos = posicao.toString().split(",");
					double latitude = Double.parseDouble(separaPontos[0]);
					double longitude = Double.parseDouble(separaPontos[1]);
					LatLng lat = new LatLng(latitude, longitude);
					adicionarMarcador(map, lat, ponto);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void adicionarMarcador(GoogleMap map, LatLng latLng,
			JSONObject motorista) throws JSONException {
		MarkerOptions markerOptions = new MarkerOptions();
		markerOptions.position(latLng).title(motorista.getString("nome"))
				.snippet(motorista.getString("celular"));
		markerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.ic_taxi_passageiro));

		Marker marker = map.addMarker(markerOptions);

		motoristas.put(marker.getId(), motorista);

		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker marker) {

			}
		});

		// Customiza a janela ao clicar em um marcador
		map.setInfoWindowAdapter(new InfoWindowAdapter() {
			@Override
			public View getInfoWindow(Marker marker) {
				LinearLayout linear = (LinearLayout) this
						.getInfoContents(marker);
				// Borda imagem 9-patch
				linear.setBackgroundResource(R.drawable.janela_marker);
				return linear;
			}

			@Override
			public View getInfoContents(Marker marker) {
				// View com o conteudo
				LinearLayout linear = new LinearLayout(getBaseContext());
				linear.setLayoutParams(new LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				linear.setOrientation(LinearLayout.VERTICAL);

				TextView t = new TextView(getBaseContext());
				t.setText("Solicitar Taxista:");
				t.setTextColor(Color.BLACK);
				t.setGravity(Gravity.CENTER);
				linear.addView(t);

				TextView tTitle = new TextView(getBaseContext());
				tTitle.setText(marker.getTitle());
				tTitle.setTextColor(Color.BLUE);
				linear.addView(tTitle);

				TextView tSnippet = new TextView(getBaseContext());
				tSnippet.setText(marker.getSnippet());
				tSnippet.setTextColor(Color.BLUE);
				linear.addView(tSnippet);

				return linear;
			}
		});
	}

	@Override
	public void onConnected(Bundle connectionHint) {

		mCurrentLocation = mLocationClient.getLastLocation();

		LatLng latLng = new LatLng(mCurrentLocation.getLatitude(),
				mCurrentLocation.getLongitude());

		final CameraPosition position = new CameraPosition.Builder()
				.target(latLng).bearing(130).tilt(0).zoom(13).build();

		CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);

		// Centraliza o mapa com animacao de 10 segundos
		map.animateCamera(update);

		// adicionaPolyline(map, latLng, latLng2);
		map.setMyLocationEnabled(true);

	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCameraChange(CameraPosition position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapClick(LatLng point) {
		// TODO Auto-generated method stub

	}

	@Override
	public void executar() throws Exception {
		//response = new TaxistaModel().listTaxiPosicao();
	}

	@Override
	public void atualizarView() {
		pgFragment.showProgress(false, mapView);
		if (response != null) {
			try {
				if (!response.contains("error")) {

					List<JSONObject> lats = new ArrayList<JSONObject>();

					JSONArray jsonArray = new JSONArray(response);

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject ponto = jsonArray.getJSONObject(i);
						lats.add(ponto);
					}
					configureMap(lats);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			AndroidUtils.showSimpleToast(TaxiMapaActivity.this,R.string.error_conexao, Toast.LENGTH_SHORT);
		}
		
	}

}
