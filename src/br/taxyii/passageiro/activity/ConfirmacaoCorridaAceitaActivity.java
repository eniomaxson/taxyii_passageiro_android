package br.taxyii.passageiro.activity;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.taxyii.library.fragment.ConfirmacaoCorridaAceita;
import br.taxyii.passageiro.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ConfirmacaoCorridaAceitaActivity extends SherlockFragmentActivity {

	private ConfirmacaoCorridaAceita confirmacao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_confirmacao_corrida_aceita);

		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		confirmacao = (ConfirmacaoCorridaAceita) getSupportFragmentManager().findFragmentById(R.id.fragmentMensagemConfirmacao);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		int opcao = item.getItemId();

		if (opcao == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
		}
		return super.onMenuItemSelected(featureId, item);
	}

}
