package br.taxyii.passageiro.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import br.taxyii.library.R;
import br.taxyii.library.utils.NotificationUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {

	private static final String TAG = "GCMService";
	NotificationCompat.Builder builder;
	private JSONObject msg;
	private String titulo_notificacao;
	private String ticket_texto;
	private String mensagem;
	private int res_icon;
	private int res_id_notificao;
	private Intent intencao_notificao;
	
	
	public GcmIntentService() {
		super("GCMIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {

			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				
				sendNotification("Deleted messages on server: "+ extras.toString());
			
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				for (int i = 0; i < 5; i++) {
					Log.i(TAG,"Working... " + (i + 1) + "/5 @ "+ SystemClock.elapsedRealtime());
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
					}
				}
				sendNotification(extras.getString("msg"));
			}
		}
		WakefulBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String string_msg) {

		try {
			msg = new  JSONObject(string_msg);
			
			if(msg.getString("tipo").equals("corrida_aceita")){
				titulo_notificacao = getBaseContext().getString(R.string.msgUsuarioCorridaConfirmadaCabecalho);
				mensagem = getBaseContext().getString(R.string.msgUsuarioCorridaConfirmadaMensagem1);
				ticket_texto = getBaseContext().getString(R.string.msgUsuarioCorridaConfirmadaMensagem3);
				mensagem.replace("?", msg.getString("taxista_nome"));
				res_icon = R.drawable.ic_launcher;
				res_id_notificao = R.string.idNotificacaoCorridaAceita;
				intencao_notificao = new Intent("MSG_CORRIDA_CONFIRMADA");
				intencao_notificao.putExtra("taxista", msg.getString("taxista_nome"));
			}
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Log.i("TESET", msg.toString());
		
		NotificationUtils.create(getBaseContext(), ticket_texto, titulo_notificacao,mensagem, res_icon, res_id_notificao, intencao_notificao);
	}

}